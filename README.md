# Site web La Fab'Brique V2.0

Fait suite à la V1.0 réalisée sous Wordpress.

_Statut: En PROD_ \ô/

Customisation d'un [Bootstrap](http://getbootstrap.com/) + thème [Freelancer](https://github.com/BlackrockDigital/startbootstrap-freelancer).

----
~~(aperçu => www.lafabbrique.org/v2)~~

# www.lafabbrique.org

## Licenses

Le thème original est placé sous licence [MIT](/LICENSE)    
Les crédits et licences des fichiers et scripts utilisés par le thème sont détaillés dans le [fichier README original](/README_orgn.md)

Les photographies et illustrations sont placées sous licence Creative Commons - Attribution 2.0-Fr ([CC-BY](https://creativecommons.org/licenses/by/2.0/fr/))

## To-do

- [x] Relecture et correction des fôtedortograff/grammaire
- [x] Mettre à jour le lien vers le GGcalendar (locations>permanences)
- [x] Intégrer l'agenda des permanences/ateliers dans la ~~page "Association"~~ documentation
- [x] Réécrire les liens vers les médias sociaux (FB, ~~D*~~, ~~GitHub~~)
- [x] Réécrire les liens vers les pages de documentation précises
- [x] ~~Tester~~ Débugger le formulaire de contact (mail) via php
- [x] Paramétrage admin documentation
- [x] "Nice URLs" sur Dokuwiki
- [x] Téléversement des pages de documentation déjà créées en local
- [x] ~~Ajouter le bandeau "Tookets CA" à la page "Partenaires/nous soutenir"~~ LE RETIRER !
- [x] Réécrire les liens vers le formulaire d'adhésion 2017/2018
- [x] ~~Ajouter les bannières HelloAsso~~ Pas utile, pas dans la charte graphique...
- [ ] Intégrer un carousel en page d'accueil
- [ ] Intégrer une partie "blog" (VenC, Grav...)
- [ ] Intégrer une carte des membres (Framacarte+trombi)
- [ ] ...
